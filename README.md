# SSG (Static Size Generator) Benchmarks

Compare multiple SSG, in multiple scenarios. The central objective is to compare speed, but eventually we can add other types of tests.

## Static Site Generators

Here are the SSGs tested.

| SSG                                    | Language | Front Matter |
| -------------------------------------- | -------- | ------------ |
| [SuCoS](https://sucos.brunomassa.com/) | C#       | YAML         |
| [Hugo](https://gohugo.io/)             | Go       | YAML         |

## FAQ

### Q: My favorite SSG is not present. How can I add it?

Check each SSG already present in [SSGBenchmarks/ssg](/SSGBenchmarks/SSGs) and also the abstract [SsgTool.cs](/SSGBenchmarks/SSGs/SsgTool.cs) class. Then, you can submit any new code that I will be glad to evaluate and add.

### Q: Do you run each SSG only once to stablish a benchmark?

Yes. _For now_ each SSG run once per scenario.
