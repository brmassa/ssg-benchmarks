using CommandLine;

namespace SSGBenchmarks;

class AppSettings
{
    public IOS OS { get; set; } = null!;

    [Option("os", Required = false, HelpText = "Operating System (currently require to run on Windwos)")]
    public OStype OSType { get; init; } = OStype.LinuxX64;

    [Option('s', "scenario", Required = false, HelpText = "Select the only scenario to run (index starts at 1)")]
    public uint Scenario { get; init; }

    [Option('f', "forceGenerateContent", Required = false, HelpText = "Force recreating the scenario base content (The default is to skip)")]
    public bool ForceGenerateContent { get; init; }

    [Option('c', "forceCopyContent", Required = false, HelpText = "Force copying the scenario content to SSG folder (The default is to skip)")]
    public bool ForceCopyContent { get; init; }

    [Option('o', "preserveOutput", Required = false, HelpText = "Preserve the output from previous runs (The default is to clear)")]
    public bool PreserveOutput { get; init; }
}
