namespace SSGBenchmarks;

class FileSystemUtilities
{
    public static IEnumerable<string> GetAllFilePaths(string directoryPath)
    {
        return Directory.GetFiles(directoryPath, "*.*", SearchOption.AllDirectories);
    }

    public static bool CompareFiles(string filePath1, string filePath2)
    {
        var file1 = File.ReadAllBytes(filePath1);
        var file2 = File.ReadAllBytes(filePath2);
        return file1.SequenceEqual(file2);
    }

    public static long CalculateDirectorySize(string directoryPath)
    {
        return Directory.GetFiles(directoryPath, "*.*", SearchOption.AllDirectories).Sum(file => new FileInfo(file).Length);
    }
}