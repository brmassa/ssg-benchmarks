using Serilog;

namespace SSGBenchmarks;

static class LinqExtensions
{
    public static async Task<SSG?> TryInstall(this SSG tool, AppSettings settings, IOS OS)
    {
        try
        {
            return await tool.Install(settings, OS);
        }
        catch (Exception ex)
        {
            Log.Information($"Error installing SSG tool '{tool.Name}': {ex.Message}");
            return null;
        }
    }
}
