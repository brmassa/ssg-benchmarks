namespace SSGBenchmarks;

class FileHelper
{
    public static void CleanDirectory(string path)
    {
        if (Directory.Exists(path))
        {
            // Delete all files from the directory
            foreach (var file in Directory.GetFiles(path))
            {
                File.Delete(file);
            }

            // Delete all child directories
            foreach (var directory in Directory.GetDirectories(path))
            {
                Directory.Delete(directory, true);
            }
        }
    }

    public static void CopyFilesRecursively(string sourcePath, string destinationPath)
    {
        if (!Directory.Exists(destinationPath))
        {
            Directory.CreateDirectory(destinationPath);
        }

        foreach (var dirPath in Directory.GetDirectories(sourcePath, "*", SearchOption.AllDirectories))
            Directory.CreateDirectory(dirPath.Replace(sourcePath, destinationPath, StringComparison.InvariantCulture));

        foreach (var newPath in Directory.GetFiles(sourcePath, "*.*", SearchOption.AllDirectories))
            File.Copy(newPath, newPath.Replace(sourcePath, destinationPath, StringComparison.InvariantCulture), true);
    }
}