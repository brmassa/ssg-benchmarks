namespace SSGBenchmarks;

class SuCoS : SSG
{
    public SuCoS()
    {
        Name = "SuCoS";
        Version = "2.3.0";
        DownloadURL = new()
        {
            { OStype.LinuxX64, "https://gitlab.com/api/v4/projects/46228669/packages/generic/sucos/v{SSG_VERSION}/sucos-linux-x64-v{SSG_VERSION}.zip" },
            { OStype.WindowsX64, "https://gitlab.com/api/v4/projects/46228669/packages/generic/sucos/v{SSG_VERSION}/sucos-win-x64-v{SSG_VERSION}.zip" },
        };
        Command = new()
        {
            { OStype.LinuxX64,  "SuCoS" },
            { OStype.WindowsX64,  "SuCoS.exe" },
        };
        // Command = ".temp/SuCoS";
        // Arguments = $"build --source \"{Path.GetFullPath(InputDirectory(0))}/\"";
        Arguments = $"build";
        OutputPath = "public";

    }
}
