namespace SSGBenchmarks;

class Hugo : SSG
{
    public Hugo()
    {
        Name = "Hugo";
        Version = "0.119.0";
        DownloadURL = new()
        {
            { OStype.LinuxX64, "https://github.com/gohugoio/hugo/releases/download/v{SSG_VERSION}/hugo_extended_{SSG_VERSION}_linux-amd64.tar.gz" },
            { OStype.WindowsX64, "https://github.com/gohugoio/hugo/releases/download/v{SSG_VERSION}/hugo_{SSG_VERSION}_windows-amd64.zip" },
        };
        Command = new()
        {
            { OStype.LinuxX64,  "hugo" },
            { OStype.WindowsX64,  "hugo.exe" },
        };
        // Command = ".temp/hugo";
        Arguments = string.Empty;
        OutputPath = "public";
    }
}
