namespace SSGBenchmarks;

class Jekyll : SSG
{
    public Jekyll()
    {
        Name = "Jekyll";
        // Command = "jekyll";
        Themes = ["default", "minimal", /* ... other themes ... */];
    }
}
