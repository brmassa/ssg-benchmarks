using System.IO.Compression;
using SharpCompress.Common;
using SharpCompress.Readers;
using Serilog;

namespace SSGBenchmarks;

/// <summary>
/// Abstract base class for SSG (Static Site Generator) tools.
/// </summary>
abstract class SSG
{
    /// <summary>
    /// Gets or sets the URL from which to download the tool.
    /// </summary>
    public required Dictionary<OStype, string> DownloadURL { get; init; }

    /// <summary>
    /// Gets or sets the command to run the tool.
    /// </summary>
    public required string Version { get; set; }

    /// <summary>
    /// Gets or sets the command to run the tool.
    /// </summary>
    public required Dictionary<OStype, string> Command { get; init; }

    /// <summary>
    /// Gets or sets the arguments to pass when running the tool.
    /// </summary>
    public required string Arguments { get; init; }

    /// <summary>
    /// Gets or sets the name of the tool.
    /// </summary>
    public required string Name { get; init; }

    /// <summary>
    /// Gets or sets the output path for the tool.
    /// </summary>
    // TODO: required keyword
    public required string OutputPath { get; init; }

    /// <summary>
    /// Gets or sets the list of themes associated with the tool.
    /// </summary>
    public required List<string> Themes { get; init; }

    /// <summary>
    /// Gets the input directory path based on the scenario.
    /// </summary>
    /// <param name="scenario">The scenario number.</param>
    /// <returns>The input directory path.</returns>
    public string InputDirectory(int scenario) => $"{ResultFolder}/{scenario}/{Name}";

    /// <summary>
    /// Gets the output directory path based on the scenario.
    /// </summary>
    /// <param name="scenario">The scenario number.</param>
    /// <returns>The output directory path.</returns>
    public string OutputDirectory(int scenario) => $"{ResultFolder}/{scenario}/{Name}/{OutputPath}";

    /// <summary>
    /// Gets the themes directory path for the tool.
    /// </summary>
    // FIXME: make these files part of the build
    public string ThemesPath => $"SSGs/{Name}/themes/";

    /// <summary>
    /// Gets the theme directory path based on the theme name.
    /// </summary>
    /// <param name="theme">The theme name.</param>
    /// <returns>The theme directory path.</returns>
    public string ThemePath(string theme) => $"{ThemesPath}/{theme}";

    private string ResultFolder => ".Result";

    public string ExecutablePath(IOS OS) => Path.Combine(Environment.CurrentDirectory, executableappDir, Command[OS.Type]);

    string executableappDir = ".SSG";

    /// <summary>
    /// Installs the SSG tool by downloading it, extracting if necessary, and making it executable.
    /// </summary>
    /// <remarks>
    /// This method downloads the tool from the specified URL, extracts it if it's a .tar.gz file,
    /// and makes it executable using chmod (on Linux) or WSL (Windows Subsystem for Linux).
    /// </remarks>
    public async Task<SSG?> Install(AppSettings settings, IOS OS)
    {
        // Check if the executable already exists
        if (!File.Exists(ExecutablePath(OS)))
        {
            await Download(settings, OS);
        }
        return OS.MakeExecutable(ExecutablePath(OS), Name) ? this : null;
    }

    private async Task Download(AppSettings settings, IOS OS)
    {
        Log.Information("{File} not found. Trying to download it...", Name);

        var replacedUrl = DownloadURL[OS.Type]
            .Replace("{SSG_VERSION}", Version, StringComparison.Ordinal);
        var fileName = Path.GetFileName(new Uri(replacedUrl).LocalPath);
        using var httpClient = new HttpClient();
        var content = await httpClient.GetByteArrayAsync(replacedUrl);
        await File.WriteAllBytesAsync(fileName, content);

        Directory.CreateDirectory(executableappDir);

        try
        {
            Decompress(fileName, executableappDir);
        }
        finally
        {
            File.Delete(fileName);
        }
    }

    private void Decompress(string fileName, string outputFolder)
    {
        if (fileName.EndsWith(".zip", StringComparison.OrdinalIgnoreCase))
        {
            DecompressZip(fileName, outputFolder);
        }
        else if (fileName.EndsWith(".tar.gz", StringComparison.OrdinalIgnoreCase))
        {
            DecompressTARGZ(fileName, outputFolder);
        }
        else
        {
            throw new Exception($"Failed to find the extracted {Name} executable.");
        }
    }

    private static void DecompressTARGZ(string fileName, string outputFolder)
    {
        using Stream stream = File.OpenRead(fileName);
        using var reader = ReaderFactory.Open(stream);
        while (reader.MoveToNextEntry())
        {
            if (!reader.Entry.IsDirectory)
            {
                var opt = new ExtractionOptions
                {
                    ExtractFullPath = true,
                    Overwrite = true
                };
                reader.WriteEntryToDirectory(outputFolder, opt);
            }
        }
    }

    private static void DecompressZip(string fileName, string outputFolder)
    {
        ZipFile.ExtractToDirectory(fileName, outputFolder, true);
    }
}
