using System.Text.Json.Serialization;

namespace SSGBenchmarks;

class Scenario(int pageCount, bool includeTags, bool includeImages)
{
    public int PageCount { get; set; } = pageCount;
    public bool IncludeTags { get; set; } = includeTags;
    public bool IncludeImages { get; set; } = includeImages;

    [JsonIgnore]
    public string Description => $"{PageCount} pages, {(IncludeTags ? "with" : "without")} tags, {(IncludeImages ? "with" : "without")} images";
}

