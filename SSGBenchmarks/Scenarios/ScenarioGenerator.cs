using System.Globalization;
using YamlDotNet.Serialization;

namespace SSGBenchmarks;

class ScenarioGenerator
{
    public string GeneratePageContent(int i, bool includeImages)
    {
        var content = $"## Post {i}\n";
        if (includeImages)
        {
            content += "![Sample Image](/images/sample.jpg)\n";
        }
        content += $"Lorem ipsum...\n\n";
        return content;
    }

    public string GenerateYamlFrontMatter(Dictionary<string, object> frontmatter)
    {
        var serializer = new SerializerBuilder().Build();
        return $"---\n{serializer.Serialize(frontmatter)}---\n\n";
    }

    public void GenerateMarkdownPages(string path, int count, bool includeTags, bool includeImages)
    {
        Directory.CreateDirectory(path);

        for (var i = 0; i < count; i++)
        {
            var formattedPostNumber = i.ToString($"D{count.ToString(CultureInfo.InvariantCulture).Length}", CultureInfo.InvariantCulture);
            var title = $"Post-{formattedPostNumber}";

            Dictionary<string, object> frontmatter = new()
            {
                { "Title", title },
                { "Date", DateTime.Now.AddDays(-i).ToString("yyyy-MM-dd", CultureInfo.InvariantCulture) },
                { "Tags", new List<string>(){"sample", "test"} },
            };
            var fileContent = GenerateYamlFrontMatter(frontmatter)
                + GeneratePageContent(i, includeImages);

            File.WriteAllText($"{path}/{title}.md", fileContent);
        }
    }
}