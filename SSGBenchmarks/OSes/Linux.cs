using System.Diagnostics;

namespace SSGBenchmarks;

class Linux : IOS
{
    public string Title => "Linux";

    public OStype Type => OStype.LinuxX64;

    public bool MakeExecutable(string Command, string programName)
    {
        using var chmodProcess = new Process()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "chmod",
                Arguments = $"+x {Command}"
            }
        };
        try
        {
            chmodProcess.Start();
        }
        catch (System.ComponentModel.Win32Exception)
        {
            throw new System.ComponentModel.Win32Exception($"Failed to make '{Command}' binary executable using '{Title}'");
        }

        chmodProcess.WaitForExit();

        if (chmodProcess.ExitCode != 0)
        {
            throw new System.ComponentModel.Win32Exception($"Failed to make '{Command}' binary executable using '{Title}'");
        }

        return true;
    }
}
