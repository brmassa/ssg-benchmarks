using System.Diagnostics;
using Serilog;

namespace SSGBenchmarks;

interface IOS
{
    public string Title { get; }
    public OStype Type { get; }

    abstract bool MakeExecutable(string Command, string programName);

    public void GenerateSite(SSG tool, int scenario)
    {
        var startInfo = new ProcessStartInfo
        {
            FileName = tool.ExecutablePath(this),
            Arguments = tool.Arguments,
            WorkingDirectory = tool.InputDirectory(scenario),
            RedirectStandardOutput = true,
            RedirectStandardError = true,
            UseShellExecute = false,
            CreateNoWindow = true,
        };

        using var process = new Process { StartInfo = startInfo };

        process.OutputDataReceived += (sender, args) => Log.Information($"{args.Data}");
        process.ErrorDataReceived += (sender, args) => Log.Information($"{args.Data}");

        process.Start();
        process.BeginOutputReadLine();
        process.BeginErrorReadLine();
        process.WaitForExit();

        if (process.ExitCode != 0)
        {
            var errorMessage = process.StandardError.ReadToEnd();
            throw new Exception($"{tool.Name} build failed with error message: {errorMessage}");
        }
    }

    public void RunCommand(string command)
    {
        using var process = new Process()
        {
            StartInfo = new ProcessStartInfo
            {
                FileName = "bash",
                Arguments = $"-c \"{command}\"",
                RedirectStandardOutput = true,
                UseShellExecute = false,
                CreateNoWindow = true,
            }
        };

        process.Start();
        process.WaitForExit();
    }
}
