using System.Diagnostics;
using Serilog;

namespace SSGBenchmarks;

class Windows : IOS
{
    public string Title => "Windows";

    public OStype Type => OStype.WindowsX64;

    public bool MakeExecutable(string Command, string programName)
    {
        return true;
    }
}
