﻿using CommandLine;
using Serilog;
using Serilog.Sinks.SpectreConsole;

namespace SSGBenchmarks;
class Program
{
    public static async Task<int> Main(string[] args)
    {
        Log.Logger = new LoggerConfiguration()
            .MinimumLevel.Debug()
            .Enrich.FromLogContext()
            .WriteTo.Console(formatProvider: System.Globalization.CultureInfo.CurrentCulture)
            // .WriteTo.Spectre("{Timestamp:HH:mm:ss} [{Level:u4}] [green]{Message:lj}{NewLine}{Exception}[/]")
            .WriteTo.Async(logger =>
            {
                logger.File(".Logs/log.txt", formatProvider: System.Globalization.CultureInfo.CurrentCulture);
            })
            .CreateLogger();

        return await Parser.Default.ParseArguments<AppSettings>(args)
            .WithParsed((settings) =>
            {
                settings.OS = OSConvert(settings.OSType);
            })
            .MapResult(
                Run,
                errs => Task.FromResult(1));
    }

    public static async Task<int> Run(AppSettings settings)
    {
        var setup = Setup.Load();
        var scenarioGenerator = new ScenarioGenerator();
        var ssgTools = await setup.LoadTools(settings, settings.OS);
        var scenarios = setup.Scenarios;

        var benchmarkRunner = new BenchmarkRunner(scenarioGenerator, ssgTools, scenarios, settings);
        benchmarkRunner.Run();
        return 0;
    }

    private static IOS OSConvert(OStype osOption) => osOption switch
    {
        OStype.LinuxX64 => new Linux(),
        OStype.WindowsX64 => new Windows(),
        _ => throw new ArgumentException("Invalid Operating System option")
    };
}
