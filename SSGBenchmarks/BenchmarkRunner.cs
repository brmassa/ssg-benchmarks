using System.Diagnostics;
using Serilog;
using Spectre.Console;

namespace SSGBenchmarks;

class BenchmarkRunner(ScenarioGenerator scenarioGenerator, List<SSG> ssgTools, List<Scenario> scenarios, AppSettings appSettings)
{
    public void Run()
    {
        WelcomeMessage();

        if (ssgTools.Count == 0)
        {
            Log.Error("No valid SSG found");
            return;
        }

        var reports = new Reports();

        for (var s = 0; s < scenarios.Count; s++)
        {
            if (appSettings.Scenario == 0 || s == appSettings.Scenario - 1)
            {
                RunScenario(appSettings, reports, s);
            }
        }

        reports.FinalReport();
    }

    private void WelcomeMessage()
    {
        Table tableSSGs = new()
        {
            Title = new($"Static Site Generators: {ssgTools.Count}")
        };
        tableSSGs.AddColumn("#");
        tableSSGs.AddColumn("SSG");
        tableSSGs.AddColumn("Version");
        for (var i = 0; i < ssgTools.Count; i++)
        {
            tableSSGs.AddRow(
                new Text($"{i + 1}"),
                new Text(ssgTools[i].Name, Reports.DataColor),
                new Text(ssgTools[i].Version, Reports.DataColor)
            );
        }
        AnsiConsole.Write(tableSSGs);

        Table tableScenarios = new()
        {
            Title = new(text: $"Scenarios: {scenarios.Count}")
        };
        tableScenarios.AddColumn("#");
        tableScenarios.AddColumn("Description");
        for (var i = 0; i < scenarios.Count; i++)
        {
            tableScenarios.AddRow(
                new Text($"{i + 1}", Reports.DataColor),
                new Text(scenarios[i].Description, Reports.DataColor)
            );
        }
        AnsiConsole.Write(tableScenarios);
    }

    private void RunScenario(AppSettings appSettings, Reports reports, int s)
    {
        var scenario = scenarios[s];

        AnsiConsole.Write(new Rule($"Running scenario: {scenario.Description}"));

        // Generate the content
        var sourcePath = $".GeneratedContent/{s}";
        if (!Path.Exists(sourcePath) || appSettings.ForceGenerateContent)
        {
            FileHelper.CleanDirectory(path: sourcePath);
            scenarioGenerator.GenerateMarkdownPages(
                sourcePath,
                scenario.PageCount,
                scenario.IncludeTags,
                scenario.IncludeImages);
        }

        var baselineTool = ssgTools.First(tool => tool.Name == "SuCoS");

        foreach (var tool in ssgTools)
        {
            RunSSGinScenario(reports, s, scenario, sourcePath, baselineTool, tool, appSettings);
        }
    }

    private static void RunSSGinScenario(Reports reports, int s, Scenario scenario, string sourcePath, SSG baselineTool, SSG tool, AppSettings appSettings)
    {
        var reportResult = new Result
        {
            SSG = tool,
            Scenario = scenario,
        };

        // FIXME: generate the content inside a blog folder
        // TODO: get the proper folder per SSG
        var toolInputPath = Path.Combine(tool.InputDirectory(s), "content", "blog");
        if (!Path.Exists(toolInputPath) || appSettings.ForceCopyContent)
        {
            // TODO: create a list of folders to skip argument (to )
            FileHelper.CleanDirectory(tool.InputDirectory(s));
            FileHelper.CopyFilesRecursively(sourcePath, toolInputPath);
        }
        // TODO: make a command line to deal with
        FileHelper.CopyFilesRecursively(Path.Combine(tool.ThemesPath, "default"), $"{tool.InputDirectory(s)}/");

        var toolOutputPath = Path.Combine(tool.OutputDirectory(s));
        if (Path.Exists(toolOutputPath) && !appSettings.PreserveOutput)
        {
            FileHelper.CleanDirectory(toolOutputPath);
        }

        var watch = Stopwatch.StartNew();
        appSettings.OS.GenerateSite(tool, s);
        watch.Stop();
        reportResult.TotalSeconds = watch.Elapsed.TotalSeconds;

        var comparer = new Comparison(baselineTool
            .OutputDirectory(s), tool.OutputDirectory(s), reportResult);
        comparer.Compare();

        reports.Results.Add(reportResult);
    }
}
