using System.Text.Json;
using Serilog;

namespace SSGBenchmarks;

record Setup
{
    public List<(string, string)> SSGTools { get; } = [
        ("SSGBenchmarks.SuCoS", "2.3.0"),
        ("SSGBenchmarks.Hugo", "0.123.7"),
        ];
    public List<Scenario> Scenarios { get; } = [
        new(100, false, false),
        new(1_000, false, false),
        // new(10_000, false, false),
        // new(100_000, false, false)
        ];

    const string setupFile = "config.json";

    public static void Save(Setup setup)
    {
        var setupJsonOut = JsonSerializer.Serialize(setup, new JsonSerializerOptions { WriteIndented = true });
        File.WriteAllText(setupFile, setupJsonOut);
    }

    public static Setup Load()
    {
        Setup? setup;
        try
        {
            var setupJson = File.ReadAllText(setupFile);
            setup = JsonSerializer.Deserialize<Setup>(setupJson);
        }
        catch (FileNotFoundException)
        {
            Log.Information($"'{setupFile}' not found. Using default setup.");
        }
        catch (JsonException ex)
        {
            Log.Information($"Error: Failed to deserialize '{setupFile}'. Using default setup. Error: {ex.Message}");
        }
        catch (Exception ex)
        {
            Log.Information($"Error: An unexpected error occurred while reading and deserializing '{setupFile}'. Using default setup. Error: {ex.Message}");
        }
        finally
        {
            setup = new Setup();
        }

        return setup;
    }

    /// <summary>
    /// Instantiate SSG tools based on the configuration
    /// </summary>
    /// <returns></returns>
    public async Task<List<SSG>> LoadTools(AppSettings settings, IOS OS) =>
        (
            await Task.WhenAll(SSGTools
                .Select(async toolData => await LoadAndTryInstall(toolData, settings, OS)))
        )
        .OfType<SSG>()
        .ToList();

    private static async Task<SSG?> LoadAndTryInstall((string, string) toolData, AppSettings settings, IOS OS)
    {
        var (name, version) = toolData;
        var type = Type.GetType(name);
        if (type is null) return null;
        var tool = (SSG?)Activator.CreateInstance(type);
        if (tool is null)
        {
            return null;
        }
        tool.Version = version;
        return await tool.TryInstall(settings, OS);
    }
}