using System.Globalization;
using System.Text;

namespace SSGBenchmarks;

class Result
{
    public required SSG SSG { get; init; }
    public required Scenario Scenario { get; init; }
    public double TotalSeconds { get; set; }
    public int BaselineFileCount { get; set; }
    public int ComparisonFileCount { get; set; }
    public double BaselineSizeMB { get; set; }
    public double ComparisonSizeMB { get; set; }
    public List<string> MissingFiles { get; set; } = [];
    public List<string> ExtraFiles { get; set; } = [];
    public List<string> DifferingFiles { get; set; } = [];

    StringBuilder sb = new();

    public string Print(ushort level = 2)
    {
        if (level > 1)
            return $"{SSG.Name}\t\t0.0.0\t\t{TotalSeconds:F5}\t\t{ComparisonFileCount}\t\t{ComparisonSizeMB:F3}";

        if (level > 2)
            CompareBaseline();

        if (level > 3)
            CompareBaselineFiles();
        return sb.ToString();
    }

    public void CompareBaseline()
    {
        sb.AppendLine(CultureInfo.InvariantCulture, $"\t\t{SSG.Name}\t\tBaseline");
        // 1. Compare number of files
        sb.AppendLine(CultureInfo.InvariantCulture, $"Number files:\t{ComparisonFileCount}\t\t{BaselineFileCount}");

        // 2. Compare total size
        sb.AppendLine(CultureInfo.InvariantCulture, $"Size:\t\t{ComparisonSizeMB:F2} KB\t\t{BaselineSizeMB:F2} KB");
    }

    private void CompareBaselineFiles()
    {
        // 3. Missing or extra files
        sb.AppendLine("Missing files:");
        foreach (var file in MissingFiles)
        {
            sb.AppendLine(file);
        }

        sb.Append("Extra files:");
        foreach (var file in ExtraFiles)
        {
            sb.AppendLine(file);
        }

        // 4. Different content
        sb.AppendLine("Files with different content:");
        foreach (var file in DifferingFiles)
        {
            sb.AppendLine(file);
        }
    }
}
