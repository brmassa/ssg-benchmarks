using System.Globalization;
using Serilog;
using Spectre.Console;

namespace SSGBenchmarks;

class Reports
{
    public DateTime Timestamp { get; set; } = DateTime.Now;
    public List<Result> Results { get; set; } = [];
    public static Color DataColor = Color.Cyan1;

    public void FinalReport()
    {
        AnsiConsole.Record();
        AnsiConsole.Write(new Rule("Results"));

        var scenario = 0;

        Results
            .GroupBy(result => result.Scenario)
            .ToList()
            .ForEach(group =>
            {
                Table table = new()
                {
                    Title = new(text: $"{++scenario} - {group.FirstOrDefault()?.Scenario.Description}", DataColor)
                };

                table.AddColumn("SSG");
                table.AddColumn("Version");
                table.AddColumn(new TableColumn("Speed (s)").RightAligned());
                table.AddColumn(new TableColumn("Pages").RightAligned());
                table.AddColumn(new TableColumn("Size (kB)").RightAligned());

                var minTotalSeconds = group.Min(r => r.TotalSeconds);
                var maxTotalSeconds = group.Max(r => r.TotalSeconds);
                var minComparisonSizeMB = group.Min(r => r.ComparisonSizeMB);
                var maxComparisonSizeMB = group.Max(r => r.ComparisonSizeMB);

                foreach (var result in group)
                {
                    var speedColor = result.TotalSeconds == minTotalSeconds
                        ? Color.Green
                        : result.TotalSeconds == maxTotalSeconds
                            ? Color.Red
                            : Color.Yellow;

                    var sizeColor = result.ComparisonSizeMB == minComparisonSizeMB
                        ? Color.Green
                        : result.ComparisonSizeMB == maxComparisonSizeMB
                            ? Color.Red
                            : Color.Yellow;

                    table.AddRow(
                    new Text(result.SSG.Name, DataColor),
                        new Text(result.SSG.Version, DataColor),
                        new Text($"{result.TotalSeconds:F5}", speedColor),
                        new Text(result.ComparisonFileCount.ToString(CultureInfo.InvariantCulture), DataColor),
                        new Text($"{result.BaselineSizeMB:F2}", sizeColor)
                    );
                }
                AnsiConsole.Write(table);
            });
        Log.Information(AnsiConsole.ExportText());
    }
}
