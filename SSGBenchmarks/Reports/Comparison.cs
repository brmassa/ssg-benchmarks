using Serilog;

namespace SSGBenchmarks;

class Comparison
{
    private string baselineDir;
    private string comparisonDir;
    private Result result;

    public Comparison(string baselineDir, string comparisonDir, Result result)
    {
        this.baselineDir = baselineDir;
        this.comparisonDir = comparisonDir;
        this.result = result;
    }

    public void Compare()
    {
        var baselineFiles = FileSystemUtilities.GetAllFilePaths(baselineDir);
        var comparisonFiles = FileSystemUtilities.GetAllFilePaths(comparisonDir);

        result.BaselineFileCount = baselineFiles.Count();
        result.ComparisonFileCount = comparisonFiles.Count();

        // 2. Compare total size
        var scale = 1024.0;
        result.BaselineSizeMB = FileSystemUtilities.CalculateDirectorySize(baselineDir) / scale;
        result.ComparisonSizeMB = FileSystemUtilities.CalculateDirectorySize(comparisonDir) / scale;

        // 3. Missing or extra files
        result.MissingFiles = baselineFiles.Except(comparisonFiles).Take(20).ToList();
        result.ExtraFiles = comparisonFiles.Except(baselineFiles).Take(20).ToList();

        // 4. Different content
        try
        {
            result.DifferingFiles = baselineFiles.Intersect(comparisonFiles)
                .Where(file => !FileSystemUtilities.CompareFiles(
                     file,
                    file))
                .Take(20).ToList();
        }
        catch (Exception ex)
        {
            Log.Error("Error comparing files.", ex);
        }
    }
}
